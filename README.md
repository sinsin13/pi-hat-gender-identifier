# Pi Hat Gender Identifier

This project is to create a PiHat that has the functionality of determining a person's gender by analyzing the frequency of their voice. It uses a microphone to input the audio from a person speaking, filtering the frequency of the input to the human auditory frequency range in order to remove noise and unwanted frequencies. From here, the frequency is separated into male and female, and identifies and records the gender of the user.

This PiHat attaches to a RaspBerry Pi Zero, and will be powered by the RaspBerry Pi Zero. To use this PiHat, you need to already have a RaspBerry Pi and it has to be connected to power. The Gender Identifier attaches to the 40 Pin board, and more information on the Pinout can be found on the RaspBerry Pi Zero datasheet.

To buld this RaspBerry PiHat, you need to have access to the following materials: 50, 1k, 10k, 4.7k ohm resistors 5.6nF capacitors small microphone - recommended is the a14032200ux0277 microphone which can be found here: https://www.amazon.com/a14032200ux0277-Microphone-Condenser-Electronic-Components/dp/B00LUUH9QW 3 LED's of different colors with approximately 2V drop across them individually.

It is best to follow the README exactly and to use the same components in order to minimize confusion and errors. However, we welcome innovation, and if you can find ways to improve the PiHat, feel free to contact us on our personal emails: sinsin@gmail.com rayray@gmail.com samsam@gmail.com
